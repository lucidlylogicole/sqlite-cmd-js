# SQLite CMD JS
A simple (nodejs) JavaScript wrapper for the [sqlite3 cli](https://sqlite.org/cli.html).

## Installation
1. Download [sqlite_cmd.js](sqlite_cmd.js) to your desired location
2. Download [sqlite3-tools](https://www.sqlite.org/download.html), extract the `sqlite3` binary and place in the same folder as `sqlite_cmd.js`

## Usage

    let sqlite = require('./sqlite_cmd.js')
    db = sqlite.SQLiteDB('test.db')
    let res = db.cmd("SELECT * FROM users")
    // res = [{id:1, name:'Jack'},{id:2, name:'Bart'}]

## API
**SQLiteDB(db_path)** - the main database object
- `db_path` (str) - location of the database
- **.cmd(cmd, params)** - sql command to execute with optional parameters. Returns a JSON object if the command is a query
    - `cmd` (str) - the sql command
    - `params` (dict) - optional named parameters if using them in the cmd
- **.cmds(cmds)** - run a bunch of commands
    - `cmds` (array) - an array of cmd strings or an array of an array with layout: `[command string, params]`
- **.cli(cmd)** - specify [cli](https://sqlite.org/cli.html) commands
    - `cmd` (str) - one or more cli commands. separate commands by `\n`
- **.tables()** - return a list of the tables
- **.mode** (str) - set the output mode (default is `'json'`)
- **.headers** (str) - turn the headers on or off (default is undefined which ignores this setting)
    - `on` - output headers on
    - `off` - output headers off
    - `undefined` - ignore this setting
- **.sqlite_path** (str) - the path to the sqlite binary (default location is `./sqlite3`)

## Examples

Setup the database

    let sqlite = require('./sqlite_cmd.js')
    let db = sqlite.SQLiteDB('test.db')

Create a table

    cmd = "CREATE table users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)"
    db.cmd(cmd)

Insert a row

    db.cmd("INSERT INTO users (name) VALUES ('Bob')")

Insert a row using parameters

    params = {user_name:'Mark'}
    db.cmd("INSERT INTO users (name) VALUES (:user_name)", params)

Select the table (returns JSON)

    result = db.cmd("SELECT * FROM users")
    // [ { id: 1, name: 'Bob' }, { id: 2, name: 'Mark' } ]

Select using parameter

    result = db.cmd("SELECT id FROM users WHERE name=:user_name", {user_name:'Mark'})
    // [ { id: 2 } ]

Insert Multiple at once

    cmds = [
        ["INSERT INTO users (name) VALUES ('Jim')"],
        ["INSERT INTO users (name) VALUES ('Drake')"],
        ["INSERT INTO users (name) VALUES (:user_name)", {user_name:'April'}],
        "SELECT * FROM users",
    ]
    result = db.cmds(cmds)

Change the mode to csv

    db.headers = 'on' // Include the column headers
    db.mode = 'csv'
    result = db.cmd("SELECT * FROM users")

Using CLI Commands directly

    let script = `.headers on
    .mode html
    SELECT * FROM users;`
    result = db.cli(script)
