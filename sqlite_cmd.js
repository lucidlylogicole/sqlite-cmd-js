const child_process = require('child_process')

function SQLiteDB(filepath) {

    //---o:
    let SQLiteDB = {
        VERSION:'1.0.1',
        filepath:filepath,
        mode: 'json',
        headers:undefined,
        sqlite_path: __dirname+'/sqlite3',
        
        tables: function() {
            return SQLiteDB.cli('.tables').trim().split(/[ ,]+/)
        },
        
        parseParams: function(params) {
            let param_cmd = '.param init\n'
            for (let p in params) {
                // Escape quotes and \n
                let param_val = (''+params[p]).replace(/[']/g,"''").replace(/["]/g,'\\"').replace(/[\n]/g,`\\n`)
                param_cmd += `.parameter set :${p} "'${param_val}'"\n`
            }
            return param_cmd
        },
        
        cmd: function(cmd, params) {
            
            // Set SQLite Settings
            let sqlsettings = `.mode ${SQLiteDB.mode}\n`
            if (SQLiteDB.headers !== undefined) {
                sqlsettings = `.headers ${SQLiteDB.headers}\n`+sqlsettings
            }
            if (params !== undefined) {
                sqlsettings += SQLiteDB.parseParams(params)
            }
            
            // Run Command
            let res = SQLiteDB.cli(sqlsettings+cmd)
            
            // Parse Result
            if (res) {
                if (SQLiteDB.mode == 'json') {
                    return JSON.parse(res)
                }
                return res
            } else {
                return []
            }
            
        },
        
        cmds: function(cmds) {
            
            // Set SQLite Settings
            let sqlsettings = `.mode ${SQLiteDB.mode}\n`
            if (SQLiteDB.headers !== undefined) {
                sqlsettings = `.headers ${SQLiteDB.headers}\n`+sqlsettings
            }
            
            // Join Commands together
            let cmd_join = ''
            for (let cmd of cmds) {
                let sfx = '\n'
                let cmd_str
                // Check Command type
                if (typeof(cmd) == 'string') {
                    // String Command
                    cmd_str = cmd
                } else {
                    cmd_str = cmd[0]
                    // Check for Parameters
                    if (cmd.length > 1) {
                        cmd_join += SQLiteDB.parseParams(cmd[1])
                    }
                    if (!cmd[0].endsWith(';')) {
                        sfx = ';\n'
                    }
                }
                cmd_join += cmd_str+sfx
            }
            
            // Run Commands
            let res = SQLiteDB.cli(sqlsettings+cmd_join)
            
            // Parse Result
            if (res) {
                return res
            } else {
                return []
            }
            
        },
        
        cli: function(cmd) {
            let full_cmd = SQLiteDB.sqlite_path+' '+SQLiteDB.filepath+" <<'EOS'\n"+cmd+"\nEOS"
            let res = child_process.execSync(full_cmd,{encoding:'utf8'})
            return res
        }
        
    }

    return SQLiteDB
    
}

module.exports = {SQLiteDB}